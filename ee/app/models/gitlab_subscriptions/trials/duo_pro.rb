# frozen_string_literal: true

module GitlabSubscriptions
  module Trials
    module DuoPro
      DURATION_NUMBER = 60
      DURATION = DURATION_NUMBER.days
    end
  end
end
